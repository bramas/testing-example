#include <fstream>
#include "myvector.hpp"

// This file is an example of "big" simulation
// The output of this file is tested a non regression test

int main(){
    MyVector<int> aVector{1, 2, 3, 4, 5};
    
    const int sum = aVector.sum();
    
    std::ofstream output("simulation-with-output.output");
    output << "Res = " << sum << std::endl;
    
    return 0;
}

