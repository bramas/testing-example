#include <iostream>
#include "myvector.hpp"

// This file is an example of "big" simulation
// The output of this file is tested a non regression test

int main(){
    MyVector<int> aVector{1, 2, 3, 4, 5};
    
    const int sum = aVector.sum();
    
    std::cout << "Res = " << sum << std::endl;
    
    return 0;
}

