#!/bin/bash

echo "Run $2, with parameters ${@:3}, and redirect output to $1"

$2 "${@:3}" > $1
