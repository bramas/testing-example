// Please read the corresponding licence file
#ifndef MYVECTOR_HPP
#define MYVECTOR_HPP

#include <vector>

template <class T>
class MyVector : public std::vector<T>{
public:
    using std::vector<T>::vector;
    
    T sum() const {
        T res = T();
        
        for(const auto& val : (*this)){
             res += val;
        }
        
        return res;
    }
};


#endif

