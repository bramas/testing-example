[![pipeline status](https://gitlab.inria.fr/bramas/testing-example/badges/master/pipeline.svg)](https://gitlab.inria.fr/bramas/testing-example/commits/master)
[![coverage report](https://gitlab.inria.fr/bramas/testing-example/badges/master/coverage.svg)](https://bramas.gitlabpages.inria.fr/testing-example/)

This project is simply an example to present on way of using testing
and continuous integration.

Content:
- src: The source contains only a basic vector (inherits from `std::vector`).
- tests: Two tests that are lined to the vector. `simulation.cpp` prints its results on the standard output. `simulation-with-output.cpp` prints its results in the file `./simulation-with-output.output`.
- unit-tests: Contains the unit tests based on the simple `UTest` class.
- output_references: Contains the good output files.

# Compilation

Simply go in the build dir and use cmake as usual:
```
mkdir build
cd build
# To enable testing: cmake -DUSE_TESTING=ON -DUSE_SIMU_TESTING=ON ..
cmake ..
make
```

# Running the tests

Use
```
make test
```

To get the output of the tests, use:
```
CTEST_OUTPUT_ON_FAILURE=TRUE make test
```

# Gitlab ci

The file `.gitlab-ci.yml` contains the information related to the continuous integration on gitlab.

# Coverage result

Can be found here: https://bramas.gitlabpages.inria.fr/testing-example/

