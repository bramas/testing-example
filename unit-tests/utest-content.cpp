#include "UTester.hpp"
#include "myvector.hpp"


class TestContent : public UTester< TestContent > {
    using Parent = UTester< TestContent >;
    
    void TestBasic() {
      MyVector<int> vec{0, 1, 2, 3};
      
      UASSERTETRUE(vec.size());
      
      UASSERTEEQUAL(0, vec[0]);
      UASSERTEEQUAL(1, vec[1]);
      UASSERTEEQUAL(2, vec[2]);
      UASSERTEEQUAL(3, vec[3]);
    }

    void LoopTestBasic() {
      MyVector<int> vec;
      
      for(int idx = 0 ; idx < 10 ; ++idx){
        vec.push_back(idx);
      }
      
      for(int idx = 0 ; idx < 10 ; ++idx){
          UASSERTEEQUAL(idx, vec[idx]);
      }
    }
    void SetTests() {
        Parent::AddTest(&TestContent::TestBasic, "Basic test for vector");
        Parent::AddTest(&TestContent::LoopTestBasic, "Basic loop test for vector");
    }
};

// You must do this
TestClass(TestContent)


