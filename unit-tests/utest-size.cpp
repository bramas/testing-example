#include "UTester.hpp"
#include "myvector.hpp"


class TestSize : public UTester< TestSize > {
    using Parent = UTester< TestSize >;
    
    void TestBasic() {
      {
          MyVector<int> vec{0, 1, 2, 3};
          UASSERTEEQUAL(vec.size(), 4UL);
          
          vec.pop_back();
          UASSERTEEQUAL(vec.size(), 3UL);
          
          vec.clear();
          UASSERTEEQUAL(vec.size(), 0UL);
          UASSERTETRUE(vec.empty());
      }
      {
          MyVector<int> vec;
          UASSERTEEQUAL(vec.size(), 0UL);
          UASSERTETRUE(vec.empty());
      }
    }

    void TestAdvanced() {
      MyVector<int> vec;
      
      for(int idx = 0 ; idx < 10 ; ++idx){
          vec.push_back(idx);
          UASSERTEEQUAL(vec.size(), (unsigned long)idx+1);
      }
      
      for(int idx = 10 ; idx > 0; --idx){
          vec.pop_back();
          UASSERTEEQUAL(vec.size(), (unsigned long)idx-1);
      }
    }
    void SetTests() {
        Parent::AddTest(&TestSize::TestBasic, "Basic test for vector");
        Parent::AddTest(&TestSize::TestAdvanced, "Basic loop test for vector");
    }
};

// You must do this
TestClass(TestSize)


